# cloudflare-lb-cli

### Roadmap

1. Write a simple Python CLI to manage cloudflare load balancers and add/remote targets from them 
2. Test it in CI/CD systems 
3. Migrate the CLI to Golang once it is "feature complete" enough for 
